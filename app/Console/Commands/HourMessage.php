<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class HourMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'message:hour';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Artisan Send Hour Laravel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo 'Sukses Waktu';
        // return Command::SUCCESS;
    }
}
