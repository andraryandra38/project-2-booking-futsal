<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Validation extends Model
{
    use HasFactory;

    protected $table = 'validations2';
    protected $primaryKey = 'id';
    protected $fillable = [
        'role',
        'id_valid',
        'nama',
        'hp',
        'email',
        'jambooking',
        'alamat',
        'lapangan',
        'harga',
        'credit'

    ];
}
