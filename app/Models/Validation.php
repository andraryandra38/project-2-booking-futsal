<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Validation extends Model
{
    use HasFactory;

    protected $table = 'validations';
    protected $primaryKey = 'id';
    protected $fillable =[
        'role',
        'id_valid',
        'status_booking',
        'nama',
        'hp',
        'email',
        'jambooking',
        'alamat',
        'lapangan',
        'harga',
        'credit',
        'status',

    ];
}
