<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|max:200',
            'lapangan' => 'required',
            'harga' => 'required',
            'credit' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'nama.required'    => 'Nama Wajib Diisi',
            'lapangan.required'      => 'Pilih Lapangan Futsal',
            'harga.required'     => 'Harga Tetap',
            'credit.required'     => 'Pilih Pembayaran',
        ];
    }
}
