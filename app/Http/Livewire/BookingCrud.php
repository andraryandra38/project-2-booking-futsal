<?php

namespace App\Http\Livewire;
use Livewire\Component;
use App\Models\Booking;

class BookingCrud extends Component
{
    public 
    $bookings, 
    $role,
    $id_valid, 
    $status_booking,
    $nama,
    $hp,
    $email,
    $alamat,
    $jambooking,
    $lapangan,
    $harga,
    $credit,
    $status;
    
    public $isModalOpen = 0;

    public function render()
    {
        $this->bookings = Booking::all();
        return view('livewire.booking-crud');
    }

    public function create()
    {
        $this->resetCreateForm();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isModalOpen = true;
    }

    public function closeModal()
    {
        $this->isModalOpen = false;
    }

    private function resetCreateForm(){
        $this->role = '';
        $this->id_valid = '';
        $this->status_booking = '';
        $this->nama = '';
        $this->hp = '';
        $this->email = '';
        $this->alamat = '';
        $this->jambooking = '';
        $this->lapangan = '';
        $this->harga = '';
        $this->credit = '';
        $this->status = '';

    }
    
    public function store()
    {
        $this->validate([
            'role' => 'required',
            'id_valid' => 'required',
            'status_booking' => 'required',
            'nama' => 'required',
            'hp' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'jambooking' => 'required',
            'lapangan' => 'required',
            'harga' => 'required',
            'credit' => 'required',
            'status' => 'required',
        ]);
    
        Booking::updateOrCreate(['id' => $this->booking_id], [
            'role' => $this->role,
            'id_valid' => $this->id_valid,
            'status_booking' => $this->status_booking,
            'nama' => $this->nama,
            'hp' => $this->hp,
            'email' => $this->email,
            'alamat' => $this->alamat,
            'jambooking' => $this->jambooking,
            'lapangan' => $this->lapangan,
            'harga' => $this->harga,
            'credit' => $this->credit,
            'status' => $this->status,

        ]);

        session()->flash('message', $this->booking_id ? 'Data updated successfully.' : 'Data added successfully.');

        $this->closeModal();
        $this->resetCreateForm();
    }

    public function edit($id)
    {
        $booking = Booking::findOrFail($id);
        $this->booking_id = $id;
        $this->role = $booking->role;
        $this->id_valid = $booking->id_valid;
        $this->status_booking = $booking->status_booking;
        $this->nama = $booking->nama;
        $this->hp = $booking->hp;
        $this->email = $booking->email;
        $this->alamat = $booking->alamat;
        $this->jambooking = $booking->jambooking;
        $this->lapangan = $booking->lapangan;
        $this->harga = $booking->harga;
        $this->credit = $booking->credit;
        $this->status = $booking->status;

        $this->openModal();
    }
    
    public function delete($id)
    {
        Booking::find($id)->delete();
        session()->flash('message', 'Data deleted successfully.');
    }
} 