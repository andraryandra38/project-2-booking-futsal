<?php

namespace App\Http\Controllers;

// use Barryvdh\DomPDF\PDF;
use Barryvdh\DomPDF\Facade as PDF;
// use PDF;
use App\Models\Validation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Support\Facades\Hash;

class ValidationControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Validation $valid)
    {
       $valid = Validation::latest()->paginate(10);
       $blogs = Blog::All();
       return view('valid.index', compact('valid','blogs'))->with('i', (request()->input('page', 1) -1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Validation $valid)
    {
       return view('valid.create', compact('valid'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Validation $valid)
    {
        $request->validate([
            // 'role' => 'required',
            // 'id_valid' => 'required',
            // 'status_booking' => 'required',
            // 'nama' => 'required',
            // 'hp' => 'required',
            // 'email' => 'required',
            // 'alamat' => 'required',
            // 'jambooking' => 'required',
            // 'lapangan' => 'required',
            // 'harga' => 'required',
            // 'credit' => 'required',
            // 'status' => 'required'
            

            'role' => 'required', // id role USER
            'id_valid' => 'required', // id sesuai login
            'status_booking' => 'required', // status booking
            'nama' => 'string', 'max:255', // nama pengguna
            'hp' => 'string', 'max:255', // no.handphone
            'email' => 'string', 'max:255', // email@gmail.com
            'alamat' => 'string', 'max:255', // alamat
            'jambooking' => 'required', // jam booking jam 6 s.d 23.00
            'lapangan' => 'required', // lapangan 1 dan 2
            'harga' => 'required', // harga booking
            'credit' => 'required', // dana atau ovo
            'status' => 'required', // bayar dan belum bayar

        ]);
        Validation::create($request->all());

        return redirect()->route('valid.index', compact('valid'))->with('succes', 'Data Berhasil di Input');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Validation $valid)
    {
        return view('valid.show', compact('valid'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Validation $valid)
    {
        return view('valid.edit', compact('valid'));
    }

    public function editkonfirmasi(Validation $valid)
    {
        return view('valid.editkonfirmasi', compact('valid'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Validation $valid)
    {
        $request->validate([
            'role' => 'required',
            'id_valid' => 'required',
            'status_booking' => 'required',
            'nama' => 'required',
            'hp' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'jambooking' => 'required',
            'lapangan' => 'required',
            'harga' => 'required',
            'credit' => 'required',
            'status' => 'required',
  

        ]);

        $valid->update($request->all());

        return redirect()->route('valid.index', compact('valid'))->with('succes', 'Data Berhasil di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Validation $valid)
    {
        $valid->delete();

        return redirect()->route('valid.index', compact('valid'))->with('succes', 'Data Berhasil di Hapus');
    }

    public function export()
    {
        $valid = Validation::all();

        //mengambil data dan tampilan dari halaman laporan_pdf
        //data di bawah ini bisa kalian ganti nantinya dengan data dari database
        $valid = PDF::loadview('valid.laporan_pdf', ['valid' => $valid])->setPaper('A4', 'landscape')->setOptions(['defaultFont' => 'sans-serif']);
        //mendownload laporan.pdf
        return $valid->download('laporan-futsalbooking.pdf');
    }

}
