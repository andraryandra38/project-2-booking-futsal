<?php

namespace App\Http\Controllers;


use App\Models\Validation;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\App;

class LaporanController extends Controller
{
    public function index()
    {
        $valid = Validation::all();
        //menampilkan halaman laporan
        return view('laporan', compact('valid'))->with('i', (request()->input('page', 1) -1) * 5);
    }

    public function export()
    {
        $valid = Validation::all();

        //mengambil data dan tampilan dari halaman laporan_pdf
        //data di bawah ini bisa kalian ganti nantinya dengan data dari database
        $valid = PDF::loadview('laporan_pdf2', ['valid' => $valid])->setPaper('A4', 'landscape')->setOptions(['defaultFont' => 'sans-serif']);
        //mendownload laporan.pdf
        return $valid->download('laporan.pdf');
    }


//     public function export()
//     {
//         $valid = Validation::first();
//         $valid = PDF::loadview('valid.show', ['valid' => $valid])->setPaper('A4', 'landscape')->setOptions(['defaultFont' => 'sans-serif']);
//         //mendownload laporan.pdf
//         return $valid->download('laporan.pdf');
//  }

}
