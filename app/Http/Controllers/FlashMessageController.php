<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FlashMessageController extends Controller
{
    public function index()
    {
        return view('pesan');
    }

    public function pesan()
    {
        // return redirect('/pesan')->with(['success' => 'Pesan Berhasil']);
        // return redirect('/pesan')->with(['warning' => 'Pesan Warning']);
        // return redirect('/pesan')->with(['info' => 'Pesan Info']);
        return redirect('/pesan')->with(['error' => 'Pesan Error']);
    }
}
