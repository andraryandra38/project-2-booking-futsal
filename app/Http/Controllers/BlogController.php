<?php

namespace App\Http\Controllers;

use App\Models\Blog;
// use App\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Validation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        $blogs = Blog::latest()->paginate(10);
        $valid = Validation::All();
        // $blogs = Auth::First()->Auth()->User()->role == admin;

        // return view('blog.index', compact('blogs'));
        // return view('blog.index-admin', compact('blogs'));

        if ($blogs == '0') {
            return view('blog.index-admin', compact('blogs','valid'));
        } else {
            return view('blog.index', compact('blogs','valid'));
        }

    }

    public function create()
    {
        return view('blog.create');
    }


    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image'     => 'required|image|mimes:png,jpg,jpeg',
            'role'     => 'required',
            'title'     => 'required',
            'userid'     => 'required',
            // 'content'   => 'required'
        ]);

        //upload image
        $image = $request->file('image');
        $image->storeAs('public/blogs', $image->hashName());

        $blog = Blog::create([
            'image'     => $image->hashName(),
            'role'     => $request->role,
            'title'     => $request->title,
            'userid'     => $request->userid,
            
            // 'content'   => $request->content
        ]);

        if ($blog) {
            //redirect dengan pesan sukses
            return redirect()->route('blog.index')->with(['success' => 'Data Berhasil Disimpan!']);
            return redirect()->route('blog.index-admin')->with(['success' => 'Data Berhasil Disimpan!']);

        } else {
            //redirect dengan pesan error
            return redirect()->route('blog.index')->with(['error' => 'Data Gagal Disimpan!']);
            return redirect()->route('blog.index-admin')->with(['error' => 'Data Gagal Disimpan!']);

        }
    }

    public function edit(Blog $blog)
    {
        return view('blog.edit', compact('blog'));
    }


    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $blog
     * @return void
     */
    public function update(Request $request, Blog $blog)
    {
        $this->validate($request, [
            'role'     => 'required',
            'title'     => 'required',
            'userid'     => 'required',
            // 'content'   => 'required'
        ]);

        //get data Blog by ID
        $blog = Blog::findOrFail($blog->id);

        if ($request->file('image') == "") {

            $blog->update([
                'role'     => $request->role,
                'title'     => $request->title,
                'userid'     => $request->userid,
                // 'content'   => $request->content
            ]);
        } else {

            //hapus old image
            Storage::disk('local')->delete('public/blogs/' . $blog->image);

            //upload new image
            $image = $request->file('image');
            $image->storeAs('public/blogs', $image->hashName());

            $blog->update([
                'image'     => $image->hashName(),
                'role'     => $request->role,
                'title'     => $request->title,
                'userid'     => $request->userid,
                // 'content'   => $request->content
            ]);
        }

        if ($blog) {
            //redirect dengan pesan sukses
            return redirect()->route('blog.index')->with(['success' => 'Data Berhasil Diupdate!']);
            return redirect()->route('blog.index-admin')->with(['success' => 'Data Berhasil Diupdate!']);
            
        } else {
            //redirect dengan pesan error
            return redirect()->route('blog.index')->with(['error' => 'Data Gagal Diupdate!']);
            return redirect()->route('blog.index-admin')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }
    public function destroy($id)
    {
        $blog = Blog::findOrFail($id);
        Storage::disk('local')->delete('public/blogs/' . $blog->image);
        $blog->delete();

        if ($blog) {
            //redirect dengan pesan sukses
            return redirect()->route('blog.index')->with(['success' => 'Data Berhasil Dihapus!']);
            return redirect()->route('blog.index-admin')->with(['success' => 'Data Berhasil Dihapus!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('blog.index')->with(['error' => 'Data Gagal Dihapus!']);
            return redirect()->route('blog.index-admin')->with(['error' => 'Data Gagal Dihapus!']);
        }
    }
}
