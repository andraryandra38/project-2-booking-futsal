<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use NascentAfrica\Jetstrap\JetstrapFacade;
class AppServiceProvider extends ServiceProvider
// use Illuminate\Support\ServiceProvider;

{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Schema::defaultstringlength(191);
         JetstrapFacade::useAdminLte3();

    }
}
