<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>
<link rel="stylesheet" href="{{ asset('/css/logo.css') }}">

        <script src="https://kit.fontawesome.com/fbbd9413d8.js" crossorigin="anonymous"></script>
        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" placeholder="Email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>
            
            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password"  placeholder="Password Minimalize 8 Character..." class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
            </div>

            <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <input id="remember_me" type="checkbox" class="form-checkbox" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif
            </div>
            <br>
              <center><x-jet-button class="mr-3 pr-3 pl-3 p-2 ">
                    {{ __('Login') }}
                </x-jet-button></center>
        
            <div class="logo">
                <a href="{{ url('authorized/google') }}" id="btn-gllogin" style="margin: 15px;">
                    <i class="fab fa-google fa-4x">
                        <p >Google</p>
                    </i>
                </a>
       
                <a href="{{ url('register') }}" id="btn-fblogin" style="margin: 15px; text-decoration: none; color: green;">
                <i class="fas fa-registered fa-4x"></i>
                    <p>Register</p>
                </i>
                </a>

                <a href="{{ url('redirect') }}" id="btn-fblogin" style="margin: 15px; ">
                <i class="fab fa-facebook fa-4x">
                    <p>Facebook</p>
                </i>
                </a>
            </div>

            
        </form>
    </x-jet-authentication-card>
</x-guest-layout>