<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>
        <link rel="stylesheet" href="{{ asset('/css/logo.css') }}">
          <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <script src="https://kit.fontawesome.com/fbbd9413d8.js" crossorigin="anonymous"></script>


        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

             <div>
                {{-- <x-jet-label for="role" value="{{ __('role') }}" /> --}}
                <x-jet-input id="role" class="block mt-1 w-full" type="hidden" name="role" value="0" : required autofocus autocomplete="role" />
            </div>
            {{-- <div> --}}
                {{-- <x-jet-label for="hp" value="{{ __('hp') }}" /> --}}
                 {{-- <x-jet-input id="hp" class="block mt-1 w-full" type="hidden" name="hp" value="0" :  autofocus autocomplete="hp" />
            </div> 
             <div>  --}}
                {{-- <x-jet-label for="alamat" value="{{ __('alamat') }}" /> --}}
                {{-- <x-jet-input id="alamat" class="block mt-1 w-full" type="hidden" name="alamat" value="0" :  autofocus autocomplete="hp" />
            </div> --}}

            <div>
                <x-jet-label for="name" value="{{ __('Name') }}" />
                <x-jet-input id="name" placeholder="Name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            </div>

            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" placeholder="Email@gmail.com" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>
            {{-- <div class="mt-4">
                <x-jet-label for="hp" value="{{ __('No.HandPhone') }}" />
                <x-jet-input id="hp" class="block mt-1 w-full" type="hp" name="hp" :value="old('hp')" required />
            </div> --}}
            {{-- <div class="mt-4">
                <x-jet-label for="alamat" value="{{ __('Alamat') }}" />
                <x-jet-input id="alamat" class="block mt-1 w-full" type="alamat" name="alamat" :value="old('alamat')" required />
            </div> --}}

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" placeholder="Password Minimalize 8 Character..." class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                <x-jet-input id="password_confirmation" placeholder="Password Minimalize 8 Character..." class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

            @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                <div class="mt-4">
                    <x-jet-label for="terms">
                        <div class="flex items-center">
                            <x-jet-checkbox name="terms" id="terms"/>

                            <div class="ml-2">
                                {!! __('I agree to the :terms_of_service and :privacy_policy', [
                                        'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Terms of Service').'</a>',
                                        'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Privacy Policy').'</a>',
                                ]) !!}
                            </div>
                        </div>
                    </x-jet-label>
                </div>
            @endif

            <div class="flex items-center justify-end mt-4 mr-7">
                <a href="{{ route('login') }}">
                   <button type="button" class="btn btn-warning"> {{ __('Already registered?') }}
                </button></a>

                <x-jet-button class="ml-4">
                    {{ __('Register') }}
                </x-jet-button>
            </div>
        </form>

         <div class="logo">
                <a href="{{ url('authorized/google') }}" id="btn-gllogin" style="margin: 15px;">
                    <i class="fab fa-google fa-4x">
                        <p>Google</p>
                    </i>
                </a>
       
                <a href="{{ url('redirect') }}" id="btn-fblogin" style="margin: 15px;">
                <i class="fab fa-facebook fa-4x">
                    <p>Facebook</p>
                </i>
                </a>
            </div>

        {{-- @if (JoelButcher\Socialstream\Socialstream::show())
            <x-socialstream-providers />
        @endif --}}
    </x-jet-authentication-card>
</x-guest-layout>
