<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Booking') }}
        </h2>
    </x-slot>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap CSS -->
        {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> --}}
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
{{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> --}}
        <title>Booking Futsal</title>
 
  </head>
    <body>
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
    <li class="breadcrumb-item active" aria-current="page">Booking</li>
  </ol>
</nav>

        <div class="container my-5">
            <h2 class="fs-4 fw-bold text-center">Booking Futsal</h2>
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    @if (session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('success') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    <form action="data" method="post">
                        @csrf
                        <div class="mb-3">
                            <label for="nama" class="form-label">Nama:</label>
                            <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" id="nama" value="{{ old('nama') }} {{ Auth()->User()->name }} ">
                            @error('nama')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="hp" class="form-label">No.HandPhone:</label>
                            <input type="text" name="hp" class="form-control @error('hp') is-invalid @enderror" id="hp" value="{{ old('hp') }} ">
                            @error('hp')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        <div class="mb-3">
                            <label for="email" class="form-label">Email:</label>
                            <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }} {{ Auth()->User()->email }} ">
                            @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="alamat" class="form-label">alamat:</label>
                            <input type="text" name="alamat" class="form-control @error('alamat') is-invalid @enderror" id="alamat" value="{{ old('alamat') }} ">
                            @error('alamat')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                     <div class="mb-3">
                            <label for="jambooking" class="form-label">Jam Booking:</label>
                           <br>
                            <select class="btn btn-danger" name="jambooking" class="form-control @error('jambooking') is-invalid @enderror" id="jambooking" value="{{ old('jambooking') }}">
                            <option selected>Pilih Jam Booking...</option>
                             <option value="06:00">06:00</option>
                             <option value="07:00">07:00</option>
                             <option value="08:00">08:00</option>
                             <option value="09:00">09:00</option>
                             <option value="10:00">10:00</option>
                             <option value="11:00">11:00</option>
                             <option value="12:00">12:00</option>
                             <option value="13:00">13:00</option>
                             <option value="14:00">14:00</option>
                             <option value="15:00">15:00</option>
                             <option value="16:00">16:00</option>
                             <option value="17:00">17:00</option>
                             <option value="18:00">18:00</option>
                             <option value="19:00">19:00</option>
                             <option value="20:00">20:00</option>
                             <option value="21:00">21:00</option>
                             <option value="22:00">22:00</option>
                             <option value="23:00">23:00</option>
                         </select>
                            @error('jambooking')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>


                        <div class="mb-3">
                            <label for="lapangan" class="form-label">Lapangan:</label>
                           <br>
                            <select class="btn btn-primary" name="lapangan" class="form-control @error('lapangan') is-invalid @enderror" id="lapangan" value="{{ old('lapangan') }}">
                            <option selected>Pilih Lapangan...</option>
                             <option value="Lapangan 1">lapangan1</option>
                                <option value="Lapangan 2">lapangan2</option>
                         </select>
                            @error('lapangan')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        
         

                        
                        <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                        <div class="mb-3">
                            <label for="harga" class="form-label">Harga:</label>
                            <input type="text" name="harga" class="form-control @error('harga') is-invalid @enderror" id="harga"  readonly="disabled" value="{{ old(' harga') }} Rp.45.000 ">
                            @error('harga')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        
                           <div class="mb-3">
                            <label for="credit" class="form-label">Credit:</label>        
                            <br>
                            <select class="btn btn-success" name="credit" class="form-control @error('credit') is-invalid @enderror" id="credit" value="{{ old('credit') }}">
                            <option selected>Pilih Credit...</option>
                             <option value="DANA">DANA</option>
                                <option value="OVO">OVO</option>
                         </select>
                            @error('credit')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

{{-- 
<div class="form-check">
                        <input  type="radio" name="lapangan" id="lapangan" class="form-check-input @error('lapangan') is-invalid @enderror" id="lapangan" readonly="disabled" value="{{ old('lapangan') }} Lapangan 1">
                        <label class="form-check-label">Lapangan 1</label> 
                         </label>
                        </div>     
                        <div class="form-check">
                        <input  type="radio" name="lapangan" id="lapangan" checked class="form-check-input @error('lapangan') is-invalid @enderror" id="lapangan" readonly="disabled" value="{{ old('lapangan') }} Lapangan 1">
                        <label class="form-check-label" for="credit">Lapangan 2</label>
                        @error('lapangan')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            </div> 
                        </div>
                         <div class="form-check">
                        <input  type="radio" name="credit" id="credit" class="form-check-input @error('credit') is-invalid @enderror" id="credit" readonly="disabled" value="{{ old('credit') }} DANA">
                        <label class="form-check-label">DANA</label> 
                         </label>
                        </div>     
                        <div class="form-check">
                        <input  type="radio" name="credit" id="credit" checked class="form-check-input @error('credit') is-invalid @enderror" id="credit" readonly="disabled" value="{{ old('credit') }} OVO">
                        <label class="form-check-label" for="credit">OVO</label>
                        @error('credit')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            </div> 
                        </div> --}}


                        <button type="submit" class="btn btn-primary btn-lg btn-block mt-4">Kirim</button>
                    </form>
                </div>
            </div>
        </div>
    
    </body>
</html>

</x-app-layout>