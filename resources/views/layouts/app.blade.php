<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'BookingFutsal') }}</title>

        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}">
<!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" > -->
        
        @livewireStyles
 
        <!-- Scripts -->
        <link rel="stylesheet" href="/css/app.css">
        <script src="{{ mix('js/app.js') }}" defer></script>
        <script src="{{ mix('js/dashboard.js') }}" defer></script>
        <script src="https://kit.fontawesome.com/fbbd9413d8.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" />

    </head>
    <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed font-sans antialiased">
        <div class="wrapper">
            
            <!-- Navbar -->
            @livewire('navigation-menu')
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-warning elevation-2">
                <!-- Brand Logo -->
                <a href="/" class="brand-link">
                    {{-- <x-jet-application-mark width="36" class="brand-image img-circle elevation-1" style="opacity: .8" /> --}}
                    <img src="bola1.png" alt="" width="60">
                    <span class="brand-text font-weight-light">Prima Futsal</span>
                </a>

                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                            <div class="image">
                                <img src="{{ Auth::user()->profile_photo_url }}" class="img-circle elevation-1" alt="{{ Auth::user()->name }}">
                            </div>
                        @endif
                        <div class="info">
                            <a href="{{ route('profile.show') }}" class="d-block">{{ Auth::user()->name }}</a>
                        </div>
                    </div>
{{-- /////////////////////////////////////////// --}}
                    @if (Auth()->user()->role == 'admin')    
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <a href="/users">
                        <i class="fas fa-user-plus mr-3" style="margin-left: 20px;"> </i>
                             <span class="brand-text" style="font-size: 20px">Tambah User</span>
                         </a>
                    </div>                    
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <a href="/valid">
                         <i class="fas fa-futbol fa-1x mr-3" style="margin-left: 20px;"> </i>
                             <span class="brand-text" style="font-size: 20px">Booking Futsal</span>
                         </a>
                    </div>

                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <a href="/blog">
                         <i class="flex fas fa-upload monospace mr-3" style="margin-left: 20px;"> </i>
                             <span class="brand-text" style="font-size: 20px">Upload Bukti DP</span>
                         </a>
                    </div>

                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <a href="/laporan2">
                        <i class="fas fa-file-pdf mr-4" style="margin-left: 20px;"> </i>
                             <span class="brand-text" style="font-size: 20px">Laporan</span>
                         </a>
                    </div>
                    @endif
                    @if (Auth()->user()->role == '0')                        
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <a href="/blog">
                         <i class="flex fas fa-upload monospace mr-3" style="margin-left: 20px;"> </i>
                             <span class="brand-text" style="font-size: 20px">Upload Bukti DP</span>
                         </a>
                    </div>

                      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <a href="/valid">
                         <i class="fas fa-futbol fa-1x mr-3" style="margin-left: 20px;"> </i>
                             <span class="brand-text" style="font-size: 20px">Booking Futsal</span>
                         </a>
                    </div>

        
                     {{-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <a href="/laporan">
                         <i class="fas fa-futbol fa-1x mr-3" style="margin-left: 20px;"> </i>
                             <span class="brand-text" style="font-size: 20px">Laporan</span>
                         </a>
                    </div> --}}
                    @endif
                    
                        

                    <!-- isi BOX -->
                    <div class="d-grid gap-2 col-6 mx-auto">
                        
                    @include('navbar-dashboard/navbar-kiri')
                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column nav-legacy" data-widget="treeview" role="menu" data-accordion="false">
                            <!-- Add icons to the links using the .nav-icon class
                                 with font-awesome or any other icon font library -->

                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col">
                                <h1>{{ $header }}</h1>
                            </div>
                        </div>
                    </div><!-- /.container-fluid -->
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col">
                                {{ $slot }}
                            </div>

                            @if (isset($aside))
                                <div class="col-lg-3">
                                    {{ $aside }}
                                </div>
                            @endif
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            {{-- <footer class="main-footer">
                <div class="float-right d-none d-sm-block">
                    <b><a href="https://jetstream.laravel.com">Jetstream</a></b>
                </div>
                <strong>Powered by</strong> <a href="https://adminlte.io">AdminLTE</a>
            </footer> --}}
        </div>

        @stack('modals')
        @livewireScripts
        @stack('scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    </body>
</html>
