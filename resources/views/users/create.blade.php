@extends('users.layout')
  
@section('content')
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Tambah User
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('users.store') }}" id="myForm">
            @csrf
            {{-- <div class="form-group">
                    <label for="role">UserID</label>                    
                    <input type="text" name="role" class="form-control" id="role" aria-describedby="role" >                
                </div>     --}}

                  <div class="mb-3">
                            <label for="role" class="form-label">UserID</label>        
                            <br>
                            <select class="btn btn-success" name="role" class="form-control @error('role') is-invalid @enderror" id="role" value="{{ old('role') }}">
                            <option aria-readonly="disabled" selected>UserID...</option>
                             <option value="admin">Admin</option>
                                <option value="0">User</option>
                         </select>
                            @error('role')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
            
                <div class="form-group">
                    <label for="name">Name</label>                    
                    <input type="text" name="name" class="form-control" id="name" aria-describedby="name" >                
                </div>
                {{-- <div class="form-group">
                    <label for="hp">No.HandPhone</label>                    
                    <input type="text" name="hp" class="form-control" id="hp" aria-describedby="hp" >                
                </div> --}}
                <div class="form-group">
                    <label for="email">Email</label>                    
                    <input type="email" name="email" class="form-control" id="email" aria-describedby="email" >                
                </div>
                 {{-- <div class="form-group">
                    <label for="alamat">Alamat</label>                    
                    <input type="text" name="alamat" class="form-control" id="alamat" aria-describedby="alamat" >                
                </div> --}}
                        <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>
           <br>
            <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            </div>
        </div>
    </div>
    </div>
@endsection