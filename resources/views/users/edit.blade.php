@extends('users.layout')
  
@section('content')
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Edit User
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('users.update', $user->id) }}" id="myForm">
            @csrf
            @method('PUT')
              <div class="mb-3">
                            <label for="role" class="form-label">UserID</label>        
                            <br>
                            <select class="btn btn-success" name="role" class="form-control @error('role') is-invalid @enderror" id="role">
                            <option aria-readonly="disabled" selected for="role" name="role" id="role">UserID...</option>
                             <option value="admin">Admin</option>
                                <option value="0">User</option>
                         </select>
                            @error('role')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                <div class="form-group">
                    <label for="name">Name</label>                    
                    <input type="text" name="name" class="form-control" id="name" value="{{ $user->name }}" aria-describedby="name" >                
                </div>
                 {{-- <div class="form-group">
                    <label for="hp">No.Handphone</label>                    
                    <input type="text" name="hp" class="form-control" id="hp" value="{{ $user->hp }}" aria-describedby="hp" >                
                </div> --}}
                <div class="form-group">
                    <label for="email">Email</label>                    
                    <input type="text" name="email" class="form-control" id="email" value="{{ $user->email }}" aria-describedby="email" >                
                </div>
                {{-- <div class="form-group">
                    <label for="alamat">Alamat</label>                    
                    <input type="text" name="alamat" class="form-control" id="alamat" value="{{ $user->alamat }}" aria-describedby="alamat" >                
                </div> --}}
                <div class="form-group">
                    <label for="writer">Password</label>                    
                    <input type="password" name="password" class="form-control" id="password" value="{{ $user->password }}" aria-describedby="password" >                
                </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection