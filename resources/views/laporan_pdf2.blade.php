
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan</title>
</head>
<body>
        <h2 style="text-decoration: underline; margin: 10px;">Laporan Booking Futsal</h2>
    
<table class="table table-bordered">
        <tr>

            <!-- <th width="70px"class="text-center">No</th> -->
            <th width="70px"class="text-center">Nama</th>
            <th width="70px"class="text-center">No.HandPhone</th>
            <th width="70px" class="text-center">Email</th>
            <th width="70px" class="text-center">Alamat</th>
            <th width="70px" class="text-center">Jam Booking</th>
            <th width="70px" class="text-center">Lapangan</th>
            <th width="70px" class="text-center">Harga</th>
            <th width="70px" class="text-center">Credit</th>
            <th width="70px" class="text-center">Tanggal</th>
 

        </tr>
        @foreach ($valid as $validation)
         @if (Auth()->User()->id == $validation->id_valid || Auth()->User()->role == 'admin')
            
        <tr>

        <!-- <td style="text-align: center;">{{ $validation->id }}</td> -->
            <td style="text-align: center;">{{ $validation->nama }}</td>
            <td style="text-align: center;">{{ $validation->hp }}</td>
            <td style="text-align: center;">{{ $validation->email }}</td>
            <td style="text-align: center;">{{ $validation->alamat }}</td>
            <td style="text-align: center;">{{ $validation->jambooking }}</td>
            <td style="text-align: center;">{{ $validation->lapangan }}</td>
            <td style="text-align: center;">{{ $validation->harga }}</td>
            <td style="text-align: center;">{{ $validation->credit }}</td>
            <td style="text-align: center;">{{ $validation->created_at }}</td>
 
        </tr>
        @endif

        @endforeach
    </table>

</body>
</html>