@extends('template')

@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Edit Data</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('valid.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('valid.update',$valid->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {{-- <strong>Role:</strong> --}}
                <input type="hidden" name="role" class="form-control" readonly="disabled" placeholder="Role" value="{{ $valid->role }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Id_Valid:</strong>
                <input type="text" name="id_valid" class="form-control" readonly="disabled" placeholder="Id_valid" value="{{ $valid->id_valid }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama:</strong>
                <input type="text" name="nama" class="form-control" placeholder="Nama" value="{{ $valid->nama }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>No.HandPhone:</strong>
                <input type="text" name="hp" class="form-control" placeholder="HP" value="{{ $valid->hp }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                <input type="text" name="email" class="form-control" placeholder="Email" value="{{ $valid->email }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Alamat:</strong>
                <input type="text" name="alamat" class="form-control" placeholder="Alamat" value="{{ $valid->alamat }}">
            </div>
        </div>
        
         <div class="mb-3">
                            <label for="jambooking" class="form-label">Jam Booking:</label>
                           <br>
                            <select class="btn btn-danger" name="jambooking" class="form-control @error('jambooking') is-invalid @enderror" id="jambooking" value="{{ old('jambooking') }}">
                            <option selected>Pilih Jam Booking...</option>
                             <option value="06:00">06:00</option>
                             <option value="07:00">07:00</option>
                             <option value="08:00">08:00</option>
                             <option value="09:00">09:00</option>
                             <option value="10:00">10:00</option>
                             <option value="11:00">11:00</option>
                             <option value="12:00">12:00</option>
                             <option value="13:00">13:00</option>
                             <option value="14:00">14:00</option>
                             <option value="15:00">15:00</option>
                             <option value="16:00">16:00</option>
                             <option value="17:00">17:00</option>
                             <option value="18:00">18:00</option>
                             <option value="19:00">19:00</option>
                             <option value="20:00">20:00</option>
                             <option value="21:00">21:00</option>
                             <option value="22:00">22:00</option>
                             <option value="23:00">23:00</option>
                         </select>
                            @error('jambooking')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

        <div div class=" ml-3 mb-3">
                            <label for="lapangan" class="form-label">Lapangan:</label>
                           <br>
                            <select class="btn btn-primary" name="lapangan" class="form-control @error('lapangan') is-invalid @enderror" id="lapangan" value="{{ old('lapangan') }}">
                            <option selected>Pilih Lapangan...</option>
                             <option value="Lapangan 1">lapangan1</option>
                                <option value="Lapangan 2">lapangan2</option>
                         </select>
                            @error('lapangan')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Harga:</strong>
                <input type="text" name="harga" class="form-control" readonly="disabled" placeholder="Harga" value="{{ $valid->harga }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Credit:</strong>
                <input type="text" name="credit" class="form-control" readonly="disabled" placeholder="Credit" value="{{ $valid->credit }}">
            </div>
        </div>



        <div class="col-xs-12 col-sm-12 col-md-12 text-center p-5">
            <button type="submit" class="btn btn-primary m-5">Update</button>
        </div>
    </div>

    </form>
@endsection