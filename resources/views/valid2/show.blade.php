@extends('template')

@section('content')

    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2> Show Data</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('valid.index') }}"> Back</a>
            </div>
        </div>
    </div>
<br>
@if (Auth()->User()->id == $valid->id_valid || Auth()->User()->role == 'admin') 
    

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Role:</strong>
                {{ $valid->role }}
            </div>
        </div>
<br>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>ID_Valid:</strong>
                {{ $valid->id_valid }}
            </div>
        </div>
<br>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama:</strong>
                {{ $valid->nama }}
            </div>
        </div>
<br>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>No.HandPhone:</strong>
                {{ $valid->hp }}
            </div>
        </div>
<br>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                {{ $valid->email }}
            </div>
        </div>
<br>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Alamat:</strong>
                {{ $valid->alamat }}
            </div>
        </div>
<br>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Jam Booking:</strong>
                {{ $valid->jambooking }}
            </div>
        </div>
<br>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Lapangan:</strong>
                {{ $valid->lapangan }}
            </div>
        </div>
<br>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Harga:</strong>
                {{ $valid->harga }}
            </div>
        </div>
<br>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>credit:</strong>
                {{ $valid->credit }}
            </div>
        </div>
<br>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tanggal:</strong>
                {{ $valid->created_at }}
            </div>
        </div>
<br>

    </div>
@endif

@endsection