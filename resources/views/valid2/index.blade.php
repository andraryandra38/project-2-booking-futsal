@extends('template')

@section('content')

    <div class="row mt-5 mb-5">
     <div class="col-lg-12 margin-tb">
         
            <div class="float-right">
                <a class="btn btn-success" href="{{ route('valid.create') }}"> Input Validation</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('succes'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    <table class="table table-bordered">
        <tr>
            {{-- <th width="20px" class="text-center">No</th> --}}
            <th width="50px" class="text-center">Role</th>
            <th width="50px"class="text-center">Id_Valid</th>
            <th width="50px"class="text-center">Nama</th>
            <th width="50px"class="text-center">No.HandPhone</th>
            <th width="50px" class="text-center">Email</th>
            <th width="50px" class="text-center">Alamat</th>
            <th width="50px" class="text-center">Jam Booking</th>
            <th width="50px" class="text-center">Lapangan</th>
            <th width="50px" class="text-center">Harga</th>
            <th width="50px" class="text-center">Credit</th>
            <th width="50px" class="text-center">Tanggal</th>
            <th width="50px" class="text-center">Action</th>

        </tr>
        @foreach ($valid as $validation)
        {{-- if(count($valid as $validation)) {  --}}
        {{-- @if (Auth()->User()->id == $validation->id_valid) --}}
        {{-- @if ($validation->id_valid || Auth()->User()->role == 'admin') --}}
         @if (Auth()->User()->id == $validation->id_valid || Auth()->User()->role == 'admin')
               
        <tr>
            {{-- <td class="text-center">{{ ++$i }}</td> --}}
            <td>{{ $validation->role }}</td>
            <td>{{ $validation->id_valid }}</td>
            <td>{{ $validation->nama }}</td>
            <td>{{ $validation->hp }}</td>
            <td>{{ $validation->email }}</td>
            <td>{{ $validation->alamat }}</td>
            <td>{{ $validation->jambooking }}</td>     
            <td>{{ $validation->lapangan }}</td>
            <td>{{ $validation->harga }}</td>
            <td>{{ $validation->credit }}</td>
            <td>{{ $validation->created_at }}</td>
            <td class="text-center">
                <form action="{{ route('valid.destroy',$validation->id) }}" method="POST">

                   <a class="btn btn-info btn-sm" href="{{ route('valid.show',$validation->id) }}">Show</a>

                    <a class="btn btn-primary btn-sm" href="{{ route('valid.edit',$validation->id) }}">Edit</a><br>

                    @csrf
                    @method('DELETE')
                    <br>
                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
                </form>
            </td>
        </tr>
        @endif

        @endforeach
    </table>

    {{-- {!! $valid->links() !!} --}}

@endsection