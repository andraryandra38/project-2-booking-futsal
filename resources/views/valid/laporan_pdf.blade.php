<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
        <table class="table table-bordered">
        <tr>
            {{-- <th width="20px" class="text-center">No</th> --}}
            <th width="10px" class="text-center">Role</th>
            <th width="10px"class="text-center">Id_Valid</th>
            <th width="10px"class="text-center">Nama</th>
            <th width="10px"class="text-center">No.HandPhone</th>
            <th width="10px" class="text-center">Email</th>
            <th width="10px" class="text-center">Alamat</th>
            <th width="10px" class="text-center">Jam Booking</th>
            <th width="10px" class="text-center">Lapangan</th>
            <th width="10px" class="text-center">Harga</th>
            <th width="10px" class="text-center">Credit</th>
            {{-- <th width="10px" class="text-center">Action</th> --}}

        </tr>
        @foreach ($valid as $validation)
         @if (Auth()->User()->id == $validation->id_valid || Auth()->User()->role == 'admin')
            
        <tr>
            {{-- <td class="text-center">{{ ++$i }}</td> --}}
            <td>{{ $validation->role }}</td>
            <td>{{ $validation->id_valid }}</td>
            <td>{{ $validation->nama }}</td>
            <td>{{ $validation->hp }}</td>
            <td>{{ $validation->email }}</td>
            <td>{{ $validation->alamat }}</td>
            <td>{{ $validation->jambooking }}</td>
            <td>{{ $validation->lapangan }}</td>
            <td>{{ $validation->harga }}</td>
            <td>{{ $validation->credit }}</td>
    
        </tr>
        @endif

        @endforeach
    </table>
</body>
</html>