@extends('template')

@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Edit Data</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('valid.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('valid.update',$valid->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {{-- <strong>Role:</strong> --}}
                <input type="hidden" name="role" class="form-control" readonly="disabled" placeholder="Role" value="{{ $valid->role }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {{-- <strong>Id_Valid:</strong> --}}
                <input type="hidden" name="id_valid" class="form-control" readonly="disabled" placeholder="Id_valid" value="{{ $valid->id_valid }}">
            </div>
        </div>
                     

        <div div class=" ml-3 mb-3">
              <label for="status" class="form-label">Status Booking:</label>
                <br>
                   <select class="btn btn-primary" name="status_booking" class="form-control @error('status_booking') is-invalid @enderror" id="status_booking" value="{{ $valid->status_booking }}">
                     <option selected value="Belum Booking">Belum Booking</option>
                          <option value="Sudah Booking">Sudah Booking</option>
                </select>
            </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama:</strong>
                <input type="text" name="nama" class="form-control" placeholder="Nama" value="{{ $valid->nama }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>No.HandPhone:</strong>
                <input type="text" name="hp" class="form-control" placeholder="HP" value="{{ $valid->hp }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                <input type="text" name="email" class="form-control" placeholder="Email" value="{{ $valid->email }}">
            </div>
        </div>

          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Alamat:</strong>
                 <textarea class="form-control" name="alamat" rows="5" placeholder="Alamat" >{{ $valid->alamat }}</textarea>
            </div>
        </div>

   @if ( Auth::User()->role == "0")

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Jam Booking:</strong>
                <input type="text" name="jambooking" class="form-control" style="background-color: rgb(196, 196, 196)" readonly="disabled" placeholder="Jam Booking"  value="{{ $valid->jambooking }}">
            </div>
        </div>

    

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Lapangan:</strong>
                <input type="text" name="lapangan" class="form-control" style="background-color: rgb(196, 196, 196)" readonly="disabled" placeholder="Lapangan"  value="{{ $valid->lapangan }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Harga:</strong>
                <input type="text" name="harga" class="form-control" style="background-color: rgb(196, 196, 196)" readonly="disabled"  placeholder="Harga"  value="{{ $valid->harga }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Credit:</strong>
                <input type="text" name="credit" class="form-control" style="background-color: rgb(196, 196, 196)" readonly="disabled"  placeholder="Credit" value="{{ $valid->credit }}">
            </div>
        </div>

          <!-- <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Status:</strong>
                <input type="text" name="status" class="form-control" style="background-color: rgb(196, 196, 196)" readonly="disabled" placeholder="Status" value="{{ $valid->status }}">
            </div>
        </div> -->

@endif



@if (Auth::User()->role == "admin") 
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Jam Booking:</strong>
                <input type="text" name="jambooking" class="form-control" placeholder="Jam Booking" value="{{ $valid->jambooking }}">
            </div>
        </div>

    

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Lapangan:</strong>
                <input type="text" name="lapangan" class="form-control"  placeholder="Lapangan"  value="{{ $valid->lapangan }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Harga:</strong>
                <input type="text" name="harga" class="form-control" placeholder="Harga" value="{{ $valid->harga }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Credit:</strong>
                <input type="text" name="credit" class="form-control"  placeholder="Credit" value="{{ $valid->credit }}">
            </div>
        </div>

          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Status DP:</strong>
                <input type="text" name="status" class="form-control" readonly="disabled" placeholder="Status" value="{{ $valid->status }}">
            </div>
        </div>
        {{-- Jika Transasi Lunas maka tidak usah tampil untuk edit pelunasannya --}}
                @if ($valid->status == "Transaksi Lunas") 
                     
                <div div class=" ml-3 mb-3">
                            <label for="status" class="form-label">Status Edit:</label>
                           <br>
                            <select class="btn btn-primary" name="status" class="form-control @error('status') is-invalid @enderror" id="status" value="{{ $valid->status }}">
                            @if ($valid->status == "Transaksi Lunas")
                        <option selected value="Transaksi Lunas">{{ $valid->status }}</option>
                             <option value="Belum Lunas">Belum Lunas</option>
                         </select>
                            @endif
                            
                        @error('lapangan')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror    
                </div>      
                @else
       {{-- Jika Belum Lunas maka tampil untuk edit pelunasannya --}}
                    <div div class=" ml-3 mb-3">
                            <label for="status" class="form-label">Status DP:</label>
                           <br>
                            <select class="btn btn-primary" name="status" class="form-control @error('status') is-invalid @enderror" id="status" value="{{ $valid->status }}">
                            <option selected value="Belum Lunas">Belum Lunas</option>
                             <option value="Transaksi Lunas">Transaksi Lunas</option>
                         </select>
                            @error('lapangan')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        @endif
                    @endif
                    </div>

                     
                    

        <div class="col-xs-12 col-sm-12 col-md-12 text-center p-5">
            <button type="submit" class="btn btn-primary m-5">Update</button>
        </div>
    </div>

    </form>
@endsection