<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Upload Bukti DP :') }}
        </h2>
    </x-slot>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Blogs - SantriKoding.com</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
</head>

<body style="background: lightgray">
    
 <b><p style="font-style: italic;">Segera Chat Pemilik! apabila sudah mengirim Bukti DP.</p></b>
 <button type="submit" class="btn btn-sm btn-success"> <i class="fab fa-whatsapp fa-2x"></i>
        <a href="https://wa.me/6285314005779?text=
  *Hallo Admin!* Nama saya *{{ Auth()->User()->name }}* ingin bertanya.
  " style="color: #ffff; text-decoration: none;">Chat Via WhatsApp</a>

</button>

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow rounded">
                    <div class="card-body">
                        <a href="{{ route('blog.create') }}" class="btn btn-md btn-success mb-3">Upload</a>
                        
             
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th scope="col">Bukti DP</th>
                                <th scope="col">Nama</th>
                                <th scope="col">UserID</th>
                                <th scope="col">Role</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Action</th>
                                
                                
                            </tr>
                            </thead>
                            <tbody>
                              @forelse ($blogs as $blog)
                                    @if (Auth()->User()->id == $blog->userid || Auth()->User()->role == 'admin')        

                                <tr>
                                    <td class="text-center">
                                        <img src="{{ Storage::url('public/blogs/').$blog->image }}" class="rounded" style="width: 150px">
                                    </td>
                                    <td>{{ $blog->title }}</td>
                                    <td>{{ $blog->userid }}</td>
                                    <td>{{ $blog->role }}</td>
                                    <td>{{ $blog->created_at }}</td>
                                  

                                    <td class="text-center">
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{ route('blog.destroy', $blog->id) }}" method="POST">
                                            <a href="{{ route('blog.edit', $blog->id) }}" class="btn btn-sm btn-primary">EDIT</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger">HAPUS</button>
                                        </form>
                                    </td>
                                </tr>
                         
                              @empty
                                  <div class="alert alert-danger">
                                      Data Blog belum Tersedia.
                                  </div>
                              @endforelse
                            </tbody>
                          </table>  
                          {{ $blogs->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <script>
        //message with toastr
        @if(session()->has('success'))
        
            toastr.success('{{ session('success') }}', 'BERHASIL!'); 

        @elseif(session()->has('error'))

            toastr.error('{{ session('error') }}', 'GAGAL!'); 
            
        @endif
    </script>

</body>
</html>

</x-app-layout>