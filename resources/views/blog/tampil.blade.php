<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Upload Bukti DP :') }}
        </h2>
    </x-slot>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Upload</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
</head>

<body style="background: lightgray">
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
    <li class="breadcrumb-item active" aria-current="page">Upload Bukti DP</li>
  </ol>
</nav>

  <b><p style="font-style: italic;">Segera Chat Pemilik! apabila sudah mengirim Bukti DP.</p></b>
 <button type="submit" class="btn btn-sm btn-success"> <i class="fab fa-whatsapp fa-2x"></i>
        <a href="https://wa.me/6285314005779?text=
  *Hallo Admin!* Nama saya *{{ Auth()->User()->name }}* ingin bertanya.
  " style="color: #ffff; text-decoration: none;">Chat Via WhatsApp</a>
</button>
<br><br>
         <button type="button" style="font-size: 25px;" class="btn btn-primary">Dana: <span class="badge" style="font-size: 20px;">085123456789</span></button>
         <button type="button" style="font-size: 25px;" class="btn btn-primary">Ovo: <span class="badge" style="font-size: 20px;">01234567890</span></button>
  
    <div class="container mt-5">
    @forelse ($blogs as $blog)
            <img src="{{ Storage::url('public/blogs/').$blog->image }}" alt="">
    @endforelse
    </div>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>


</body>
</html>

</x-app-layout>