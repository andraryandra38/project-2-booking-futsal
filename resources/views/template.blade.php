<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('') }}
        </h2>
    </x-slot>
<!DOCTYPE html>
<html>
<head>
    <title>Tutorial CRUD Laravel 8 untuk Pemula</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
       <script src="https://kit.fontawesome.com/fbbd9413d8.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" />


</head>
<body>

<div class="container">
<h2 style="text-decoration: underline;">Booking Futsal</h2>
    @yield('content')
    <br><br>
</div>

</body>
</html>
</x-app-layout>