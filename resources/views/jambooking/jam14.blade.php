@extends('template')

@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Create New Data</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('valid.index') }}"> Kembali</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Input gagal.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('valid.store') }}" method="POST">
    @csrf
@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Create New Data</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('valid.index') }}"> Kembali</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Input gagal.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('valid.store') }}" method="POST">
    @csrf

      <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {{-- <strong>Role:</strong> --}}
                <input type="hidden" name="role" class="form-control" readonly="disabled" placeholder="role" value="{{ Auth()->User()->role }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {{-- <strong>ID_Valid:</strong> --}}
                <input type="hidden" name="id_valid" class="form-control" readonly="disabled" placeholder="ID_Valid" value="{{ Auth()->User()->id }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <!-- <strong>Status_Booking:</strong> -->
                <input type="hidden" name="status_booking" class="form-control" readonly="disabled" placeholder="status_booking" value="Sudah Booking">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama:</strong>
                <input type="text" name="nama" class="form-control" placeholder="NAMA" value="{{ Auth()->User()->name }}">
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>No.HandPhone:</strong>
                <input type="text" name="hp" class="form-control" placeholder="No.HP" value="">
            </div>
        </div>
        
         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                <input type="text" name="email" class="form-control" placeholder="Email" value="{{ Auth()->User()->email }}">
            </div>
        </div>

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Alamat:</strong>
                 <textarea class="form-control" name="alamat" rows="5" placeholder="Alamat"></textarea>
            </div>
        </div>
        
     

  
          
      
         <div class="mb-3">
                            <label for="jambooking" class="form-label">Jam Booking:</label>
                           <br>
                            <select class="btn btn-danger" name="jambooking" class="form-control @error('jambooking') is-invalid @enderror" id="jambooking" value="{{ old('jambooking') }}">             
                                <option value="14:00">14.00</option>
                            
                         </select>
                            @error('jambooking')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
        <div div class=" ml-3 mb-3">
                            <label for="lapangan" class="form-label">Lapangan:</label>
                           <br>
                            <select class="btn btn-primary" name="lapangan" class="form-control @error('lapangan') is-invalid @enderror" id="lapangan" value="{{ old('lapangan') }}">
                            <option selected>Pilih Lapangan...</option>
                             <option value="Lapangan 1">lapangan1</option>
                                <option value="Lapangan 2">lapangan2</option>
                         </select>
                            @error('lapangan')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

         <div class="mb-3">
                            <label for="harga" class="form-label">Harga:</label>        
                            <br>
                            <select class="btn btn-success" name="harga" class="form-control @error('harga') is-invalid @enderror" id="harga" value="{{ old('harga') }}">
                            <option selected>Pilih Credit...</option>
                             <option value="10.000">Rp.10.000</option>
                                <option value="20.000">Rp.20.000</option>
                         </select>
                            @error('harga')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>


                     <div class="mb-3">
                            <label for="credit" class="form-label">Credit:</label>        
                            <br>
                            <select class="btn btn-success" name="credit" class="form-control @error('credit') is-invalid @enderror" id="credit" value="{{ old('credit') }}">
                            <option selected>Pilih Credit...</option>
                             <option value="DANA">DANA</option>
                                <option value="OVO">OVO</option>
                         </select>
                            @error('credit')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                
                       <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {{-- <strong>Status:</strong> --}}
                <input type="hidden" name="status" class="form-control" placeholder="Status" value='Belum Lunas' readonly="disabled">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</form>
@endsection.store

 
@endsection