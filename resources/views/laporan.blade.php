@extends('template')

@section('content')
{{-- <a href="{{ url('exportlaporan') }}" class="btn btn-success mb-4">Export PDF</a> --}}

    <div class="row mt-5 mb-5">
         <a href="{{ url('exportlaporan2') }}" class="btn btn-success mb-4"><i class="fas fa-file-pdf"> Export PDF</i></a>
        <div class="col-lg-12 margin-tb">
        
        </div>
    </div>

    <table class="table table-bordered" width="100%" style="border-collapse: collapse; border: 0px;">
        <h2 style="text-decoration: underline; margin: 10px;">Laporan Booking Futsal</h2>
        <tr>

            <!-- <th width="45px"class="text-center thead-dark" scope="col">No</th> -->
            <th width="45px"class="text-center thead-dark" scope="col">Nama</th>
            <th width="45px"class="text-center thead-dark" scope="col">No.HandPhone</th>
            <th width="45px" class="text-center thead-dark" scope="col">Email</th>
            <th width="45px" class="text-center thead-dark" scope="col">Alamat</th>
            <th width="45px" class="text-center thead-dark" scope="col">Jam Booking</th>
            <th width="45px" class="text-center thead-dark " scope="col">Lapangan</th>
            <th width="45px" class="text-center thead-dark" scope="col">Harga</th>
            <th width="45px" class="text-center thead-dark" scope="col">Credit</th>
            <th width="45px" class="text-center thead-dark" scope="col">Tanggal</th>
 

        </tr>
        @foreach ($valid as $validation)
         @if (Auth()->User()->id == $validation->id_valid || Auth()->User()->role == 'admin')
            
        <tr>

            <!-- <td style="text-align: center;">{{ $i++ }}</td> -->
            <td style="text-align: center;">{{ $validation->nama }}</td>
            <td style="text-align: center;">{{ $validation->hp }}</td>
            <td style="text-align: center;">{{ $validation->email }}</td>
            <td style="text-align: center;">{{ $validation->alamat }}</td>
            <td style="text-align: center;">{{ $validation->jambooking }}</td>
            <td style="text-align: center;">{{ $validation->lapangan }}</td>
            <td style="text-align: center;">{{ $validation->harga }}</td>
            <td style="text-align: center;">{{ $validation->credit }}</td>
            <td style="text-align: center;">{{ $validation->created_at }}</td>

        </tr>
        @endif

        @endforeach
    </table>

@endsection