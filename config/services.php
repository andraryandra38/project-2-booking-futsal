<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [
        'client_id' => '901808968416-edd40qv3sq4dihq3pllgpuk9dk64un33.apps.googleusercontent.com',
        'client_secret' => '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',
        'redirect' => 'http://localhost:8000/authorized/google/callback',
    ],

    'facebook' => [
        'client_id' => '323377279410215',
        'client_secret' => 'a18a99166af6487324d7f6849cd3d978',
        'redirect' => 'http://localhost:8000/callback',
    ],

    'github' => [
        'client_id' => '91436d9742c640a56f30',
        'client_secret' => '7ca1d07c77960affae362dfd78e1ae255b273455',
        'redirect' => 'http://localhost:8000/auth/github/callback',
    ],

];
