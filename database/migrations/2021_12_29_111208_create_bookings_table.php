<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->string('role');
            $table->string('id_valid');
            $table->string('status_booking');
            $table->string('nama');
            $table->string('hp');
            $table->string('email');
            $table->longText('alamat');
            $table->string('jambooking');
            $table->string('lapangan');
            $table->string('harga');
            $table->string('credit');
            $table->string('status');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
