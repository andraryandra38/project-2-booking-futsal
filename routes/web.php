<?php

use App\Http\Livewire\Home;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\GitHubController;
use App\Http\Controllers\LoginWithGoogleController;
use App\Http\Controllers\LoginWithFacebookController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PostRequest;
use App\Http\Controllers\ValidationControllers;
use App\Http\Controllers\FlashMessageController;
use App\Http\Livewire\BookingCrud;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', Home::class)->name('/');

Route::resource('users', UserController::class);


Route::get('auth/github', [GitHubController::class, 'gitHubLogin']);
Route::get('auth/github/callback', [GitHubController::class, 'gitHubCallback']);
Route::get('authorized/google', [LoginWithGoogleController::class, 'redirectToGoogle']);
Route::get('authorized/google/callback', [LoginWithGoogleController::class, 'handleGoogleCallback']);
Route::get('/redirect', [LoginWithFacebookController::class, 'redirectFacebook']);
Route::get('/callback', [LoginWithFacebookController::class, 'facebookCallback']);

Route::view('/booking', 'layouts.booking');
Route::view('/profile', 'layouts.profile');

Route::view('/booking-futsal', 'layouts.booking-futsal');
Route::view('/booking-jam-6', 'jambooking.jam6');
Route::view('/booking-jam-7', 'jambooking.jam7');
Route::view('/booking-jam-8', 'jambooking.jam8');
Route::view('/booking-jam-9', 'jambooking.jam9');
Route::view('/booking-jam-10', 'jambooking.jam10');
Route::view('/booking-jam-11', 'jambooking.jam11');
Route::view('/booking-jam-12', 'jambooking.jam12');
Route::view('/booking-jam-13', 'jambooking.jam13');
Route::view('/booking-jam-14', 'jambooking.jam14');
Route::view('/booking-jam-15', 'jambooking.jam15');
Route::view('/booking-jam-16', 'jambooking.jam16');
Route::view('/booking-jam-17', 'jambooking.jam17');
Route::view('/booking-jam-18', 'jambooking.jam18');
Route::view('/booking-jam-19', 'jambooking.jam19');
Route::view('/booking-jam-20', 'jambooking.jam20');
Route::view('/booking-jam-21', 'jambooking.jam21');
Route::view('/booking-jam-22', 'jambooking.jam22');
Route::view('/booking-jam-23', 'jambooking.jam23');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    Route::get('dashboard', ' App\Http\Controllers\HomeController@index');
  
    return view('dashboard');
})->name('dashboard');

Route::get('/laporan2', 'App\Http\Controllers\LaporanController@index');
Route::get('/exportlaporan2', 'App\Http\Controllers\LaporanController@export');

Route::get('/laporan', 'App\Http\Controllers\ValidationControllers@index');
Route::get('/exportlaporan', 'App\Http\Controllers\ValidationControllers@export');


Route::resource('/blog', BlogController::class);
Route::resource('/blog-admin', BlogController::class);

Route::resource('valid', ValidationControllers::class);


Route::get('/pesan', 'FlashMessageController@index');
Route::get('/get-pesan', 'FlashMessageController@pesan');

Route::view('data', 'bookingfutsal');
Route::post('data', function (App\Http\Requests\PostRequest $request) {
    $post        = new App\Models\Post();
    $post->nama = $request->nama;
    $post->hp = $request->hp;
    $post->email = $request->email;
    $post->alamat = $request->alamat;
    $post->lapangan = $request->lapangan;
    $post->harga = $request->harga;
    $post->credit = $request->credit;
    $post->save();

    // $post1 = DB::table('post')->get();

    // return back()->with('success', 'Data Added Successfully.');
    if ($post) {
        //redirect dengan pesan sukses
        return back()->with(['success' => 'Data Berhasil Disimpan!']);
    } else {
        //redirect dengan pesan error
        return back()->with(['error' => 'Data Gagal Disimpan!']);
    }
    });

Route::get('/pesan', 'App\Http\Controllers\FlashMessageController@index');
Route::get('/get-pesan', 'App\Http\Controllers\FlashMessageController@pesan');


    

    

